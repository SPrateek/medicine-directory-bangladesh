﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineDirectoryBD
{
    public class PanoramaFullScreen : Panorama
    {
        protected override System.Windows.Size MeasureOverride(System.Windows.Size availableSize)
        {
            availableSize.Width += 62;
            return base.MeasureOverride(availableSize);
        }
    }
}
