﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using MedicineDirectoryBD.Model;
using MedicineDirectoryBD.ViewModel;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using MedicineDirectoryBD.Helpers;
using Windows.Phone.Speech.Synthesis;
using System.Text;
using GalaSoft.MvvmLight.Messaging;

namespace MedicineDirectoryBD
{
    public partial class DetailsPage : PhoneApplicationPage
    {
        ViewModelLocator _locator = new ViewModelLocator();
        private Medicine foundMed = null;

        public DetailsPage()
        {
            InitializeComponent();


        }








        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var _dbViewModel = _locator.MedicineViewModel;

            if (SelectedItemPreviewGrid.DataContext == null)
            {
                string selectedIndex = "";
                if (NavigationContext.QueryString.TryGetValue("selectedItem", out selectedIndex))
                {
                    ProgressIndicatorHelper.ShowProgressIndicator(Constants.LoadingMedicineDetails);
                    int index = int.Parse(selectedIndex);

                    Task.Run(() => RetrieveSelectedMedicine(index));




                }
            }
        }

        private void RetrieveSelectedMedicine(int index)
        {
            var _dbViewModel = _locator.MedicineViewModel;
            Medicine foundMedicine = _dbViewModel.Medicines.Where(x => x.MedicineId == (index + 1)).FirstOrDefault();
            Dispatcher.BeginInvoke(() => SelectedItemPreviewGrid.DataContext = foundMedicine);
            LoadRelatedLonglistSelector(foundMedicine);
            foundMed = foundMedicine;



        }

        private void LoadRelatedLonglistSelector(Medicine medicine)
        {
            var _dbViewModel = _locator.MedicineViewModel;
            Dispatcher.BeginInvoke(() => this.AvailableAsListBox.ItemsSource = _dbViewModel.GetAvailableAsList(medicine));
            Dispatcher.BeginInvoke(() => this.ClassificationList.ItemsSource = _dbViewModel.GetClassification(medicine));
            Dispatcher.BeginInvoke(() => this.SimiliarLongListSelector.ItemsSource = (IList)_dbViewModel.GetSimiliarMedicines(medicine));
            Dispatcher.BeginInvoke(() => this.LongListSelector.ItemsSource = (IList)_dbViewModel.GetRelatedMedicines(medicine));
            Dispatcher.BeginInvoke(() => ProgressIndicatorHelper.HideProgressIndicator());

            Dispatcher.BeginInvoke(() => this.DetailsPivot.SelectedIndex = 0);
            //this.ClassificationList.ItemsSource=_dbViewModel.GetClassification(medicine);
            //this.SimiliarLongListSelector.ItemsSource = (IList)_dbViewModel.GetSimiliarMedicines(medicine);
            //this.LongListSelector.ItemsSource = (IList) _dbViewModel.GetRelatedMedicines(medicine);

        }

        private void LongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var medicine = LongListSelector.SelectedItem as Medicine;
            if (medicine != null)
            {
                SelectedItemPreviewGrid.DataContext = medicine;

                ProgressIndicatorHelper.ShowProgressIndicator(Constants.LoadingMedicineDetails);

                Task.Run(() => Dispatcher.BeginInvoke(() => SelectedItemPreviewGrid.DataContext = medicine));
                Task.Run(() => LoadRelatedLonglistSelector(medicine));

            }

        }

        private void SimiliarLongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var medicine = SimiliarLongListSelector.SelectedItem as Medicine;
            if (medicine != null)
            {
                SelectedItemPreviewGrid.DataContext = medicine;

                ProgressIndicatorHelper.ShowProgressIndicator(Constants.LoadingMedicineDetails);

                Task.Run(() => Dispatcher.BeginInvoke(() => SelectedItemPreviewGrid.DataContext = medicine));
                Task.Run(() => LoadRelatedLonglistSelector(medicine));
            }
        }

        private async void A_Click(object sender, EventArgs e)
        {
            SpeechSynthesizer synth = new SpeechSynthesizer();
            StringBuilder builder = new StringBuilder();

            if (!string.IsNullOrEmpty(this.NameTextBlock.Text))
            {
                builder.AppendLine(this.NameTextBlock.Text);

                if (!string.IsNullOrEmpty(this.Origin.Text))
                    builder.AppendLine("By origin it is " + this.Origin.Text);
                if (!string.IsNullOrEmpty(this.Manufacturer.Text))
                    builder.AppendLine("and manufactured by" + this.Manufacturer.Text);
                if (!string.IsNullOrEmpty(this.DetailsTextBlock.Text))
                    builder.AppendLine("\n" + "Details: " + this.DetailsTextBlock.Text);
                await synth.SpeakTextAsync(builder.ToString());


            }
            else
                await synth.SpeakTextAsync("Data is not loaded yet");
        }

        private void ClassificationList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.DetailsPivot.SelectedIndex = 2;
        }

        private void AddFav_Click(object sender, EventArgs e)
        {
            _locator.MedicineViewModel.AddToFavouritesAction(foundMed);
        }




    }
}