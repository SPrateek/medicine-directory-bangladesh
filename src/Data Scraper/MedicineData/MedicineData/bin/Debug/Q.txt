{
  "Name": "Q-rash",
  "Manufacturer": "Beximco",
  "OrigName": "zinc oxide",
  "Details": "Q-rash is a preparation of zinc oxide.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OTHER DERMATOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Qlev",
  "Manufacturer": "Proteety",
  "OrigName": "levofloxacin",
  "Details": "Qlev is a preparation of levofloxacin.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Qnol",
  "Manufacturer": "Decent",
  "OrigName": "ciprofloxacin",
  "Details": "Qnol is a preparation of ciprofloxacin.\r\n\r\n  Preparations containing ciprofloxacin is indicated for the treatment of infections caused by susceptible strains of the designated microorganisms in the conditions and patient populations listed below:\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Qpro",
  "Manufacturer": "Proteety",
  "OrigName": "ciprofloxacin",
  "Details": "Qpro is a preparation of ciprofloxacin.\r\n\r\n  Preparations containing ciprofloxacin is indicated for the treatment of infections caused by susceptible strains of the designated microorganisms in the conditions and patient populations listed below:\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "quazepam",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
{
  "Name": "quetiapine",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Quie",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
{
  "Name": "Qugyl",
  "Manufacturer": "Aexim",
  "OrigName": "metronidazole",
  "Details": "Qugyl is a preparation of metronidazole.\n",
  "Dosage": "",
  "Available": [
    "Qugyl 400 mg Tab.:Contains metronidazole 400 mg/tablet."
  ],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "ANTIBIOTICS AND CHEMOTHERAPEUTICS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Qugyl-400",
  "Manufacturer": "Aexim",
  "OrigName": "metronidazole",
  "Details": "Qugyl-400 is a preparation of metronidazole.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "ANTIBIOTICS AND CHEMOTHERAPEUTICS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Quiet",
  "Manufacturer": "Incepta",
  "OrigName": "quetiapine fumarate",
  "Details": "Quiet is a preparation of quetiapine fumarate.\n",
  "Dosage": "",
  "Available": [
    "Quiet 25 mg Tab. :Contains quetiapine 25 mg/tablet."
  ],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
{
  "Name": "Quin-h",
  "Manufacturer": "Hudson",
  "OrigName": "quinine",
  "Details": "Quin-h is a preparation of quinine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "quinagolide",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OTHER GYNECOLOGICALS"
  ]
}
{
  "Name": "quinapril",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "AGENTS ACTING ON THE RENIN-ANGIOTENSIN SYSTEM"
  ]
}
{
  "Name": "quinapril and diuretics",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "AGENTS ACTING ON THE RENIN-ANGIOTENSIN SYSTEM"
  ]
}
{
  "Name": "quinbolone",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANABOLIC AGENTS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "quinethazone",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DIURETICS"
  ]
}
{
  "Name": "quinethazone and potassium",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DIURETICS"
  ]
}
{
  "Name": "quingestanol",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "SEX HORMONES AND MODULATORS OF THE GENITAL SYSTEM"
  ]
}
{
  "Name": "quingestanol and estrogen",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "SEX HORMONES AND MODULATORS OF THE GENITAL SYSTEM"
  ]
}
{
  "Name": "quinidine",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CARDIAC THERAPY"
  ]
}
{
  "Name": "quinidine, combinations excl. psycholeptics",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CARDIAC THERAPY"
  ]
}
{
  "Name": "quinidine, combinations with psycholeptics",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "CARDIAC THERAPY"
  ]
}
{
  "Name": "quinine",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Cinqui",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "quinine, combinations with psycholeptics",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "OTHER DRUGS FOR DISORDERS OF THE MUSCULO-SKELETAL SYSTEM"
  ]
}
{
  "Name": "quinisocaine",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIPRURITICS, INCL. ANTIHISTAMINES, ANESTHETICS, ETC."
  ]
}
{
  "Name": "Quinoflox",
  "Manufacturer": "Healthcare",
  "OrigName": "sparfloxacin",
  "Details": "Quinoflox is a preparation of sparfloxacin.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Quinolex",
  "Manufacturer": "Globe",
  "OrigName": "chloroquine",
  "Details": "Quinolex is a preparation of chloroquine.\r\n\r\n  Chloroquine is an antimalarial drug used in the treatment and prophylaxis of malaria and treatment of hepatic amoebiasis.\n",
  "Dosage": "",
  "Available": [
    "Quinolex 250 mg Tab. :Contains chloroquine phosphate 250 mg/tablet."
  ],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Quinox",
  "Manufacturer": "Eskayef",
  "OrigName": "ciprofloxacin",
  "Details": "Quinox is a preparation of ciprofloxacin.\r\n\r\n  Preparations containing ciprofloxacin is indicated for the treatment of infections caused by susceptible strains of the designated microorganisms in the conditions and patient populations listed below:\n",
  "Dosage": "",
  "Available": [
    "Quinox 250 mg Tab.:Contains ciprofloxacin 250 mg/tablet.",
    "Quinox 500 mg Tab.:Contains ciprofloxacin 500 mg/tablet.",
    "Quinox 750 mg Tab.:Contains ciprofloxacin 750 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "quinupramine",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PSYCHOANALEPTICS"
  ]
}
{
  "Name": "quinupristin/dalfopristin",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Quixin",
  "Manufacturer": "Beacon",
  "OrigName": "levofloxacin",
  "Details": "Quixin is a preparation of levofloxacin.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Qutcpin",
  "Manufacturer": "Sun",
  "OrigName": "quetiapine fumarate",
  "Details": "Qutcpin is a preparation of quetiapine fumarate.\n",
  "Dosage": "",
  "Available": [
    "Qutcpin 25 mg Tab. :Contains quetiapine 25 mg/tablet."
  ],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
