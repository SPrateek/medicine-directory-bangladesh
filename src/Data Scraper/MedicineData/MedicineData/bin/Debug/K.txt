{
  "Name": "K mm",
  "Manufacturer": "Incepta",
  "OrigName": "phytomenadione",
  "Details": "K mm is a preparation of phytomenadione.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIHEMORRHAGICS"
  ]
}
{
  "Name": "K-one MM",
  "Manufacturer": "Square",
  "OrigName": "phytomenadione",
  "Details": "K-one MM is a preparation of phytomenadione.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIHEMORRHAGICS"
  ]
}
{
  "Name": "K-pol",
  "Manufacturer": "Modern",
  "OrigName": "paracetamol + caffeine",
  "Details": "K-pol is a preparation of paracetamol + caffeine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "K-trim",
  "Manufacturer": "Chemico",
  "OrigName": "sulfamethoxazole + trimethoprim",
  "Details": "K-trim is a preparation of sulfamethoxazole + trimethoprim.\n",
  "Dosage": "",
  "Available": [
    "K-trim 400/80 mg Tab.:Contains sulphamethoxazole 400 mg + trimethoprim 80 mg/tablet.",
    "K-trim DS 800/160 mg Tab.:Contains sulphamethoxazole 800 mg + trimethoprim 160 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kacin",
  "Manufacturer": "ACI",
  "OrigName": "amikacin",
  "Details": "Kacin is a preparation of amikacin.\r\n\r\n  Amikacin is an aminoglycoside antibiotic used in the treatment of severe Gram-negative and other infections. It is generally reserved for the treatment of severe infections caused by susceptible bacteria that are resistant to gentamicin and tobramycin.\n",
  "Dosage": "",
  "Available": [
    "Kacin 100 mg Inj.:Contains amikacin 100 mg/2 ml ampule."
  ],
  "Classification": [
    "ANTIBIOTICS AND CHEMOTHERAPEUTICS FOR DERMATOLOGICAL USE",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Kadol",
  "Manufacturer": "Chemico",
  "OrigName": "tramadol hcl",
  "Details": "Kadol is a preparation of tramadol hcl.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "kallidinogenase",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PERIPHERAL VASODILATORS"
  ]
}
{
  "Name": "Kamocillin",
  "Manufacturer": "Chemico",
  "OrigName": "ampicillin",
  "Details": "Kamocillin is a preparation of ampicillin.\r\n\r\n  Ampicillin is used in the treatment of a variety of infections. They include biliary-tract infections, bronchitis, endocarditis, gastro-enteritis (including salmonella enteritis and shigellosis), gonorrhoea, listeriosis, meningitis, perinatal streptococcal infections (intrapartum prophylaxis against group B streptococci), peritonitis, pneumonia, septicaemia, typhoid and paratyphoid fever, and urinary-tract infections.\n",
  "Dosage": "",
  "Available": [
    "Kamocillin 250 mg Cap. :Contains ampicillin 250 mg/capsule"
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Kamodrox",
  "Manufacturer": "Chemico",
  "OrigName": "null",
  "Details": "Kamodrox is a preparation of .\r\n\r\n  This combination is used to treat gastric hyper-acidity.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DRUGS FOR ACID RELATED DISORDERS"
  ]
}
{
  "Name": "Kamomide",
  "Manufacturer": "Chemico",
  "OrigName": "diloxanide furoate",
  "Details": "Kamomide is a preparation of diloxanide furoate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Kamoxy",
  "Manufacturer": "Chemico",
  "OrigName": "amoxycillin",
  "Details": "Kamoxy is a preparation of amoxycillin.\r\n\r\n  Amoxicillin is used in the treatment of infections due to susceptible (ONLY β-lactamase-negative) strains of the designated microorganisms in the conditions listed below:\n",
  "Dosage": "",
  "Available": [
    "Kamoxy250 mg Cap. :Contains amoxycillin 250 mg/capsule.",
    "Kamoxy 500 mg Cap. :Contains amoxycillin 500 mg/capsule.",
    "Kamoxy 125 mg/5 ml Susp. :Contains amoxycillin 125 mg/5 ml."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "kanamycin",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIDIARRHEALS, INTESTINAL ANTIINFLAMMATORY/ANTIINFECTIVE AGENTS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Kanaprim",
  "Manufacturer": "Globe",
  "OrigName": "primaquine phosphate",
  "Details": "Kanaprim is a preparation of primaquine phosphate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Kanaquine",
  "Manufacturer": "Globe",
  "OrigName": "quinine",
  "Details": "Kanaquine is a preparation of quinine.\n",
  "Dosage": "",
  "Available": [
    "Kanaquine 300 mg Tab. :Contains quinine sulphate 300 mg/tablet."
  ],
  "Classification": [
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Kanis",
  "Manufacturer": "Gaco",
  "OrigName": "clotrimazole",
  "Details": "Kanis is a preparation of clotrimazole.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "ANTIFUNGALS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS"
  ]
}
{
  "Name": "kaolin",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIDIARRHEALS, INTESTINAL ANTIINFLAMMATORY/ANTIINFECTIVE AGENTS"
  ]
}
{
  "Name": "Kapron",
  "Manufacturer": "Globe",
  "OrigName": "ciprofloxacin",
  "Details": "Kapron is a preparation of ciprofloxacin.\r\n\r\n  Preparations containing ciprofloxacin is indicated for the treatment of infections caused by susceptible strains of the designated microorganisms in the conditions and patient populations listed below:\n",
  "Dosage": "",
  "Available": [
    "Kapron 250 mg Tab.:Contains ciprofloxacin 250 mg/tablet.",
    "Kapron 500 mg Tab.:Contains ciprofloxacin 500 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Kdrine",
  "Manufacturer": "Opsonin",
  "OrigName": "procyclidine",
  "Details": "Kdrine is a preparation of procyclidine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTI-PARKINSON DRUGS"
  ]
}
{
  "Name": "kebuzone",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "Kefdrin",
  "Manufacturer": "GlaxoSmithKline",
  "OrigName": "cefradine",
  "Details": "Kefdrin is a preparation of cefradine.\r\n\r\n  Cefradine, a semisynthetic cephalosporin antibiotic, is indicated for the treatment of patients with mild to moderate infections caused by susceptible strains of the designated microorganisms in the conditions listed below:\n",
  "Dosage": "",
  "Available": [
    "Kefdrin 250 mg Cap. :Contains cephradine 250 mg/capsule.",
    "Kefdrin 500 mg Cap. :Contains cephradine 500 mg/capsule."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kefen",
  "Manufacturer": "Techno",
  "OrigName": "ketoprofen",
  "Details": "Kefen is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Kefim",
  "Manufacturer": "Chemico",
  "OrigName": "cefixime",
  "Details": "Kefim is a preparation of cefixime.\r\n\r\n  Cefixime is a third-generation cephalosporin antibacterial used to treat infections due to susceptible Gram-positive and Gram-negative bacteria, including gonorrhoea and infections of the respiratory and urinary tracts.\n",
  "Dosage": "",
  "Available": [
    "Kefim 200 mg Cap.:Contains cefixim 200 mg/capsule."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Keflin",
  "Manufacturer": "Opsonin",
  "OrigName": "cefalexin",
  "Details": "Keflin is a preparation of cefalexin.\r\n\r\n  Cephalexin is a semisynthetic cephalosporin antibiotic used for the treatment of the following infections when caused by susceptible strains of the designated microorganisms:\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kefosed",
  "Manufacturer": "Cosmic",
  "OrigName": "pseudoephedrine + guaiphenesin + triprolidine",
  "Details": "Kefosed is a preparation of pseudoephedrine + guaiphenesin + triprolidine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "NASAL PREPARATIONS"
  ]
}
{
  "Name": "Kelac",
  "Manufacturer": "Chemist",
  "OrigName": "ketorolac tromethamine",
  "Details": "Kelac is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Kem",
  "Manufacturer": "Chemico",
  "OrigName": "metronidazole",
  "Details": "Kem is a preparation of metronidazole.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "ANTIBIOTICS AND CHEMOTHERAPEUTICS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Kemadrin",
  "Manufacturer": "GlaxoSmithKline",
  "OrigName": "procyclidine",
  "Details": "Kemadrin is a preparation of procyclidine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTI-PARKINSON DRUGS"
  ]
}
{
  "Name": "Kemet",
  "Manufacturer": "Chemico",
  "OrigName": "metronidazole",
  "Details": "Kemet is a preparation of metronidazole.\n",
  "Dosage": "",
  "Available": [
    "Kemet 400 mg Tab.:Contains metronidazole 400 mg/tablet."
  ],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "ANTIBIOTICS AND CHEMOTHERAPEUTICS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Kemin",
  "Manufacturer": "Chemico",
  "OrigName": "metformin hcl",
  "Details": "Kemin is a preparation of metformin hcl.\n",
  "Dosage": "",
  "Available": [
    "Kemin 500 mg Tab. :Contains metformin hydrochloride 500 mg/tablet."
  ],
  "Classification": [
    "DRUGS USED IN DIABETES"
  ]
}
{
  "Name": "Kenacol",
  "Manufacturer": "Skylab",
  "OrigName": "triamcinolone",
  "Details": "Kenacol is a preparation of triamcinolone.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "CORTICOSTEROIDS, DERMATOLOGICAL PREPARATIONS",
    "CORTICOSTEROIDS FOR SYSTEMIC USE",
    "NASAL PREPARATIONS",
    "DRUGS FOR OBSTRUCTIVE AIRWAY DISEASES",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Kenacort-a",
  "Manufacturer": "Squibb",
  "OrigName": "triamcinolone",
  "Details": "Kenacort-a is a preparation of triamcinolone.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "CORTICOSTEROIDS, DERMATOLOGICAL PREPARATIONS",
    "CORTICOSTEROIDS FOR SYSTEMIC USE",
    "NASAL PREPARATIONS",
    "DRUGS FOR OBSTRUCTIVE AIRWAY DISEASES",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Kenodol",
  "Manufacturer": "Rangs",
  "OrigName": "ketorolac tromethamine",
  "Details": "Kenodol is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [
    "Kenodol 10 mg Tab.:Contains ketorolac tromethamine  10 mg/tablet."
  ],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Keolax",
  "Manufacturer": "Beximco",
  "OrigName": "clobazam",
  "Details": "Keolax is a preparation of clobazam.\r\n\r\n  Clobazam is a long-acting benzodiazepine. Preparations containing clobazam may be used as an adjunct in the treatment of epilepsy with other antiepileptics. Clobazam preparations are also used in the short-term treatment of acute anxiety.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
{
  "Name": "Keor",
  "Manufacturer": "Rephco",
  "OrigName": "cefixime",
  "Details": "Keor is a preparation of cefixime.\r\n\r\n  Cefixime is a third-generation cephalosporin antibacterial used to treat infections due to susceptible Gram-positive and Gram-negative bacteria, including gonorrhoea and infections of the respiratory and urinary tracts.\n",
  "Dosage": "",
  "Available": [
    "Keor 200 mg Cap.:Contains cefixim 200 mg/capsule."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Keplex",
  "Manufacturer": "Sonear",
  "OrigName": "cefalexin",
  "Details": "Keplex is a preparation of cefalexin.\r\n\r\n  Cephalexin is a semisynthetic cephalosporin antibiotic used for the treatment of the following infections when caused by susceptible strains of the designated microorganisms:\n",
  "Dosage": "",
  "Available": [
    "Keplex 250 mg Cap. :Contains cephalexin 250 mg/capsule."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Keprad",
  "Manufacturer": "Sonear",
  "OrigName": "cefradine",
  "Details": "Keprad is a preparation of cefradine.\r\n\r\n  Cefradine, a semisynthetic cephalosporin antibiotic, is indicated for the treatment of patients with mild to moderate infections caused by susceptible strains of the designated microorganisms in the conditions listed below:\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Keptrix",
  "Manufacturer": "Apex",
  "OrigName": "ceftriaxone",
  "Details": "Keptrix is a preparation of ceftriaxone.\n",
  "Dosage": "",
  "Available": [
    "Keptrix 250 mg I.V. Inj.:Contains ceftriaxone 250 mg/vial. ml.",
    "Keptrix 500 mg I.V. Inj.:Contains ceftriaxone 500 mg/vial. ml.",
    "Keptrix 1 gm I.V. Inj.:Contains ceftriaxone 1 gm/vial.",
    "Keptrix 250 mg I.M. Inj.:Contains ceftriaxone 250 mg/vial.",
    "Keptrix 500 mg I.M. Inj.:Contains ceftriaxone 500 mg/vial."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kerasol",
  "Manufacturer": "Incepta",
  "OrigName": "salicylic acid",
  "Details": "Kerasol is a preparation of salicylic acid.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIFUNGALS FOR DERMATOLOGICAL USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Kerolac",
  "Manufacturer": "Gaco",
  "OrigName": "ketorolac tromethamine",
  "Details": "Kerolac is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Ketalar",
  "Manufacturer": "Popular",
  "OrigName": "ketamine",
  "Details": "Ketalar is a preparation of ketamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANESTHETICS"
  ]
}
{
  "Name": "ketamine",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Calypso",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANESTHETICS"
  ]
}
{
  "Name": "ketanserin",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIHYPERTENSIVES"
  ]
}
{
  "Name": "ketazolam",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "PSYCHOLEPTICS"
  ]
}
{
  "Name": "Ketifen",
  "Manufacturer": "Acme",
  "OrigName": "ketotifen fumerate",
  "Details": "Ketifen is a preparation of ketotifen fumerate.\n",
  "Dosage": "",
  "Available": [
    "Ketifen 1 mg Tab. :Contains ketotifen fumerate 1 mg/tablet."
  ],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Keto-a",
  "Manufacturer": "Acme",
  "OrigName": "ketoprofen",
  "Details": "Keto-a is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [
    "Keto-a 50 mg Tab.:Contains ketoprofen 50 mg/tablet.",
    "Keto-a 100 mg Tab.:Contains ketoprofen 100 mg/tablet.",
    "Keto-a 100 mg Suppo. :Contains ketoprofen 100 mg/stick."
  ],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Keto-sr",
  "Manufacturer": "Hudson",
  "OrigName": "ketoprofen",
  "Details": "Keto-sr is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Ketobe",
  "Manufacturer": "Benham",
  "OrigName": "ketorolac tromethamine",
  "Details": "Ketobe is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "ketobemidone",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "ketobemidone and antispasmodics",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Ketocon",
  "Manufacturer": "Opsonin",
  "OrigName": "ketoconazole",
  "Details": "Ketocon is a preparation of ketoconazole.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIFUNGALS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIMYCOTICS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "ketoconazole",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Ketora",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIFUNGALS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIMYCOTICS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Ketodil",
  "Manufacturer": "Techno",
  "OrigName": "ketotifen fumerate",
  "Details": "Ketodil is a preparation of ketotifen fumerate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Ketof",
  "Manufacturer": "Ibn Sina",
  "OrigName": "ketotifen fumerate",
  "Details": "Ketof is a preparation of ketotifen fumerate.\n",
  "Dosage": "",
  "Available": [
    "Ketof 1 mg Tab. :Contains ketotifen fumerate 1 mg/tablet."
  ],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Ketoflex",
  "Manufacturer": "Somatec",
  "OrigName": "ketorolac tromethamine",
  "Details": "Ketoflex is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Ketofun",
  "Manufacturer": "Amico",
  "OrigName": "ketoconazole",
  "Details": "Ketofun is a preparation of ketoconazole.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIFUNGALS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIMYCOTICS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Ketomar",
  "Manufacturer": "Incepta",
  "OrigName": "ketotifen fumerate",
  "Details": "Ketomar is a preparation of ketotifen fumerate.\n",
  "Dosage": "",
  "Available": [
    "Ketomar 1 mg Tablet :Contains Ketotifen 1 mg.",
    "Ketomar Syrup :Contains Kitotifen 1 mg/5 ml.",
    "Ketomar Eye Drops :Contains  0.25 mg/ml."
  ],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Ketonac",
  "Manufacturer": "Navana",
  "OrigName": "ketoprofen",
  "Details": "Ketonac is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [
    "Ketonac 50 mg Tab.:Contains ketoprofen 50 mg/tablet."
  ],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Ketonic",
  "Manufacturer": "Eskayef",
  "OrigName": "ketorolac tromethamine",
  "Details": "Ketonic is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [
    "Ketonic 10 mg Tab.:Contains ketorolac tromethamine  10 mg/tablet.",
    "Ketonic 10 mg/ml IM/IV Inj.:Contains ketorolac tromethamine 10 mg/ml."
  ],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "ketoprofen",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Ko",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "ketoprofen, combinations",
  "Manufacturer": null,
  "OrigName": null,
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "Ketoral",
  "Manufacturer": "Square",
  "OrigName": "ketoconazole",
  "Details": "Ketoral is a preparation of ketoconazole.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIFUNGALS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIMYCOTICS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Ketorin",
  "Manufacturer": "Orion",
  "OrigName": "ketorolac tromethamine",
  "Details": "Ketorin is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [
    "Ketorin 10 mg Tab.:Contains ketorolac tromethamine  10 mg/tablet."
  ],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "ketorolac",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Tora",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Ketotif",
  "Manufacturer": "Delta",
  "OrigName": "ketotifen fumerate",
  "Details": "Ketotif is a preparation of ketotifen fumerate.\n",
  "Dosage": "",
  "Available": [
    "Ketotif 1 mg Tab. :Contains ketotifen fumerate 1 mg/tablet."
  ],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "ketotifen",
  "Manufacturer": null,
  "OrigName": "elated Brand Name: Alari",
  "Details": "\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Ketromin",
  "Manufacturer": "Pacific",
  "OrigName": "ketorolac tromethamine",
  "Details": "Ketromin is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Ketron",
  "Manufacturer": "ACI",
  "OrigName": "ketoprofen",
  "Details": "Ketron is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [
    "Ketron 50 mg Tab.:Contains ketoprofen 50 mg/tablet.",
    "Ketron SR 100 mg Cap.:Contains ketoprofen 100 mg/capsule."
  ],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Ketron d",
  "Manufacturer": "ACI",
  "OrigName": "dexketoprofen",
  "Details": "Ketron d is a preparation of dexketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "Ketron sr",
  "Manufacturer": "ACI",
  "OrigName": "ketoprofen",
  "Details": "Ketron sr is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Kevil",
  "Manufacturer": "Chemico",
  "OrigName": "loratadine",
  "Details": "Kevil is a preparation of loratadine.\n",
  "Dosage": "",
  "Available": [
    "Kevil 10 mg Tab.:Contains loratadine 10 mg/tablet."
  ],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kevilon",
  "Manufacturer": "Opsonin",
  "OrigName": "chlorhexidine ",
  "Details": "Kevilon is a preparation of chlorhexidine .\r\n\r\n  Clorhexidine is used as surgical hand scrub, healthcare personnel handwash, patient preoperative skin preparation and skin wound and general skin cleansing.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "BLOOD SUBSTITUTES AND PERFUSION SOLUTIONS",
    "ANTISEPTICS AND DISINFECTANTS",
    "MEDICATED DRESSINGS",
    "THROAT PREPARATIONS",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Kevirub",
  "Manufacturer": "Opsonin",
  "OrigName": "chlorhexidine ",
  "Details": "Kevirub is a preparation of chlorhexidine .\r\n\r\n  Clorhexidine is used as surgical hand scrub, healthcare personnel handwash, patient preoperative skin preparation and skin wound and general skin cleansing.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "BLOOD SUBSTITUTES AND PERFUSION SOLUTIONS",
    "ANTISEPTICS AND DISINFECTANTS",
    "MEDICATED DRESSINGS",
    "THROAT PREPARATIONS",
    "OPHTHALMOLOGICALS",
    "OTOLOGICALS",
    "OPHTHALMOLOGICAL AND OTOLOGICAL PREPARATIONS"
  ]
}
{
  "Name": "Kezid",
  "Manufacturer": "Chemico",
  "OrigName": "gliclazide",
  "Details": "Kezid is a preparation of gliclazide.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "DRUGS USED IN DIABETES"
  ]
}
{
  "Name": "Kflam",
  "Manufacturer": "Apex",
  "OrigName": "ketorolac tromethamine",
  "Details": "Kflam is a preparation of ketorolac tromethamine.\n",
  "Dosage": "",
  "Available": [
    "Kflam 10 mg Tab.:Contains ketorolac tromethamine  10 mg/tablet."
  ],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Khabar Saline",
  "Manufacturer": "Ibn Sina",
  "OrigName": "sodium, potassium, glucose",
  "Details": "Khabar Saline is a preparation of sodium, potassium, glucose.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIDIARRHEALS, INTESTINAL ANTIINFLAMMATORY/ANTIINFECTIVE AGENTS"
  ]
}
{
  "Name": "Kidcef",
  "Manufacturer": "Beacon",
  "OrigName": "cefpodoxime proxetil",
  "Details": "Kidcef is a preparation of cefpodoxime proxetil.\r\n\r\n  Cefpodoxime is used in the treatment of infections due to susceptible organisms in following disease conditions :\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kiddi",
  "Manufacturer": "Renata",
  "OrigName": "vitamin A, D, B1, B2, B6, C, E, nicotinamide & cod liver oil",
  "Details": "Kiddi is a preparation of vitamin A, D, B1, B2, B6, C, E, nicotinamide & cod liver oil.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Kidovit",
  "Manufacturer": "Amico",
  "OrigName": "multivitamin, plain",
  "Details": "Kidovit is a preparation of multivitamin, plain.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Kids-B",
  "Manufacturer": "Gaco",
  "OrigName": "zinc sulfate",
  "Details": "Kids-B is a preparation of zinc sulfate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Mineral Supplements"
  ]
}
{
  "Name": "Kilbac",
  "Manufacturer": "Incepta",
  "OrigName": "cefuroxime",
  "Details": "Kilbac is a preparation of cefuroxime.\n",
  "Dosage": "",
  "Available": [
    "Kilbac 125 mg Tablet :Contains Cefuroxime 125 mg.",
    "Kilbac 250 mg Tablet :Contains Cefuroxime 250 mg.",
    "Kilbac 500 mg Tablet :Contains Cefuroxime 500 mg.",
    "Kilbac Suspension :Contains Cefuroxime 125 mg/5 ml.",
    "Kilbac DS Suspension :Contains Cefuroxime 25o mg/5 ml.",
    "Kilbac 250 mg Injection:Contains cefuroxime 250 mg/vial.",
    "Kilbac 750 mg Injection:Contains Cefuroxime 750 mg/vial."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kilmax",
  "Manufacturer": "Eskayef",
  "OrigName": "cefuroxime",
  "Details": "Kilmax is a preparation of cefuroxime.\n",
  "Dosage": "",
  "Available": [
    "Kilmax 125 mg Tab:Contains cefuroximel 125 mg/tablet.",
    "Kilmax 250 mg Tab:Contains cefuroxime 250 mg/tablet.",
    "Kilmax 500 mg Tab:Contains cefuroxime 500 mg/tablet.",
    "Kilmax 125 mg/5 ml Susp.:Contains cefuroxime 125 mg/5 ml.",
    "Kilmax 250 mg Inj.:Contains cefuroxime 250 mg/vial."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kilpro",
  "Manufacturer": "Techno",
  "OrigName": "metronidazole",
  "Details": "Kilpro is a preparation of metronidazole.\n",
  "Dosage": "",
  "Available": [
    "Kilpro 400 mg Tab.:Contains metronidazole 400 mg/tablet."
  ],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "ANTIBIOTICS AND CHEMOTHERAPEUTICS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Kitex",
  "Manufacturer": "Square",
  "OrigName": "dexketoprofen",
  "Details": "Kitex is a preparation of dexketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "Klaricid",
  "Manufacturer": "Abbott",
  "OrigName": "clarithromycin",
  "Details": "Klaricid is a preparation of clarithromycin.\r\n\r\n  Clarithromycin is a semi-synthetic macrolide antibiotic. Preparations containing clarithromycin are used for the treatment of mild to moderate infections caused by susceptible strains of the designated microorganisms in the conditions as listed below:\n",
  "Dosage": "",
  "Available": [
    "Klaricid XL 500 mg Tab.:Contains clarithromycin 500 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Klion",
  "Manufacturer": "Ambee",
  "OrigName": "metronidazole",
  "Details": "Klion is a preparation of metronidazole.\n",
  "Dosage": "",
  "Available": [
    "Klion 200 mg Tab.:Contains metronidazole 200 mg/tablet.",
    "Klion 400 mg Tab.:Contains metronidazole 400 mg/tablet.",
    "Klion 125 mg/5 ml Susp.:Contains metronidazole 125 mg/5 ml."
  ],
  "Classification": [
    "STOMATOLOGICAL PREPARATIONS",
    "ANTIBIOTICS AND CHEMOTHERAPEUTICS FOR DERMATOLOGICAL USE",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "ANTIPROTOZOALS"
  ]
}
{
  "Name": "Knock",
  "Manufacturer": "Proteety",
  "OrigName": "paracetamol/acetaminophen",
  "Details": "Knock is a preparation of paracetamol/acetaminophen.\r\n\r\n  Paracetamol/acetaminophen is a pain reliever and fever reducer. It temporarily reduces fever\r\ntemporarily relieves minor aches and pains due to: headache, muscular aches, the common cold, minor pain of arthritis, backache, toothache, premenstrual and menstrual cramps.\n",
  "Dosage": "",
  "Available": [
    "Knock 500 mg Tab.:Contains paracetamol/acetaminophen 500 mg/tablet."
  ],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Knock plus",
  "Manufacturer": "Proteety",
  "OrigName": "paracetamol + caffeine",
  "Details": "Knock plus is a preparation of paracetamol + caffeine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANALGESICS"
  ]
}
{
  "Name": "Kofen",
  "Manufacturer": "Opsonin",
  "OrigName": "ketotifen fumerate",
  "Details": "Kofen is a preparation of ketotifen fumerate.\n",
  "Dosage": "",
  "Available": [
    "Kofen 1 mg Tab. :Contains ketotifen fumerate 1 mg/tablet."
  ],
  "Classification": [
    "ANTIHISTAMINES FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Koftex",
  "Manufacturer": "Amico",
  "OrigName": "pseudoephedrine + guaiphenesin + triprolidine",
  "Details": "Koftex is a preparation of pseudoephedrine + guaiphenesin + triprolidine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "NASAL PREPARATIONS"
  ]
}
{
  "Name": "Koftex-d",
  "Manufacturer": "Amico",
  "OrigName": "dextromethorphan + pseudoephedrine + triprolidine",
  "Details": "Koftex-d is a preparation of dextromethorphan + pseudoephedrine + triprolidine.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "COUGH AND COLD PREPARATIONS"
  ]
}
{
  "Name": "Koloride",
  "Manufacturer": "Beximco",
  "OrigName": "sodium chloride, potassium chloride, sodium acetate",
  "Details": "Koloride is a preparation of sodium chloride, potassium chloride, sodium acetate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": []
}
{
  "Name": "Kolosal",
  "Manufacturer": "Popular",
  "OrigName": "sodium chloride, potassium chloride, sodium acetate",
  "Details": "Kolosal is a preparation of sodium chloride, potassium chloride, sodium acetate.\n",
  "Dosage": "",
  "Available": [],
  "Classification": []
}
{
  "Name": "Konakion MM",
  "Manufacturer": "Roche",
  "OrigName": "phytomenadione",
  "Details": "Konakion MM is a preparation of phytomenadione.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIHEMORRHAGICS"
  ]
}
{
  "Name": "Kontrol tr 100",
  "Manufacturer": "Silva",
  "OrigName": "ketoprofen",
  "Details": "Kontrol tr 100 is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Kop",
  "Manufacturer": "Square",
  "OrigName": "ketoprofen",
  "Details": "Kop is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Kop sr",
  "Manufacturer": "Square",
  "OrigName": "ketoprofen",
  "Details": "Kop sr is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Kopim",
  "Manufacturer": "Square",
  "OrigName": "ketoprofen",
  "Details": "Kopim is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
{
  "Name": "Koptolin",
  "Manufacturer": "Skylab",
  "OrigName": "salbutamol",
  "Details": "Koptolin is a preparation of salbutamol.\n",
  "Dosage": "",
  "Available": [
    "Koptolin 2 mg Tab. :Contains salbutamol 2 mg/tablet.",
    "Koptolin 4 mg Tab. :Contains salbutamol 4 mg/tablet."
  ],
  "Classification": [
    "DRUGS FOR OBSTRUCTIVE AIRWAY DISEASES"
  ]
}
{
  "Name": "Krimina",
  "Manufacturer": "Modern",
  "OrigName": "levamisole",
  "Details": "Krimina is a preparation of levamisole.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTHELMINTICS"
  ]
}
{
  "Name": "Krimizole",
  "Manufacturer": "Mystic",
  "OrigName": "albendazole",
  "Details": "Krimizole is a preparation of albendazole.\r\n\r\n  Albendazole is an anthelmintic. It is used in the treatment of Neurocysticercosis (lesions caused by pork tapeworm) and Hydatid Disease (caused by dog tapeworm).\n",
  "Dosage": "",
  "Available": [
    "Krimizole DS 400 mg Tab.:Contains albendazole 400 mg/tablet.."
  ],
  "Classification": [
    "ANTHELMINTICS"
  ]
}
{
  "Name": "KT",
  "Manufacturer": "Jayson",
  "OrigName": "potassium chloride",
  "Details": "KT is a preparation of potassium chloride.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Mineral Supplements",
    "BLOOD SUBSTITUTES AND PERFUSION SOLUTIONS"
  ]
}
{
  "Name": "Kufflin",
  "Manufacturer": "Desh",
  "OrigName": "ambroxol hcl",
  "Details": "Kufflin is a preparation of ambroxol hcl.\r\n\r\n  Ambroxol is a mucolytic agent or expectorant which dissolves thick mucus and is usually used to help relieve respiratory difficulties.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "COUGH AND COLD PREPARATIONS"
  ]
}
{
  "Name": "Kumucin",
  "Manufacturer": "Kumudini",
  "OrigName": "erythromycin",
  "Details": "Kumucin is a preparation of erythromycin.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTI-ACNE PREPARATIONS",
    "ANTIBACTERIALS FOR SYSTEMIC USE",
    "OPHTHALMOLOGICALS"
  ]
}
{
  "Name": "Kuracef",
  "Manufacturer": "Sanofi-Aventis",
  "OrigName": "cefixime",
  "Details": "Kuracef is a preparation of cefixime.\r\n\r\n  Cefixime is a third-generation cephalosporin antibacterial used to treat infections due to susceptible Gram-positive and Gram-negative bacteria, including gonorrhoea and infections of the respiratory and urinary tracts.\n",
  "Dosage": "",
  "Available": [
    "Kuracef 200 mg Tab.:Contains cefixim 200 mg/tablet.",
    "Kuracef 400 mg Tab.:Contains cefixim 400 mg/tablet."
  ],
  "Classification": [
    "ANTIBACTERIALS FOR SYSTEMIC USE"
  ]
}
{
  "Name": "Kvit Gold",
  "Manufacturer": "Chemico",
  "OrigName": "multivitamin and multiminerals, A-Z gold",
  "Details": "Kvit Gold is a preparation of multivitamin and multiminerals, A-Z gold.\n",
  "Dosage": "",
  "Available": [],
  "Classification": []
}
{
  "Name": "Kvit-b",
  "Manufacturer": "Chemico",
  "OrigName": " vitamin B-complex",
  "Details": "Kvit-b is a preparation of  vitamin B-complex.\n",
  "Dosage": "",
  "Available": [
    "Kvit-b Cap.",
    "Kvit-b Tab.:"
  ],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Kvit-C",
  "Manufacturer": "Chemico",
  "OrigName": "ascorbic acid (vit C)",
  "Details": "Kvit-C is a preparation of ascorbic acid (vit C).\r\n\r\n  Vitamin C is used in the treatment and prevention of deficiency. It completely reverses symptoms of deficiency\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins",
    "GYNECOLOGICAL ANTIINFECTIVES AND ANTISEPTICS"
  ]
}
{
  "Name": "Kvit-E",
  "Manufacturer": "Chemico",
  "OrigName": "tocopherol (vit E)",
  "Details": "Kvit-E is a preparation of tocopherol (vit E).\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Kvit-M",
  "Manufacturer": "Chemico",
  "OrigName": "multivitamin, plain",
  "Details": "Kvit-M is a preparation of multivitamin, plain.\n",
  "Dosage": "",
  "Available": [
    "Kvit-M Tab.Contains vit A, B1, B2, B6, C, D, E nicotimamide and pantohenic acid."
  ],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Kvit-N",
  "Manufacturer": "Chemico",
  "OrigName": "Vitamin B1, B6 and B12",
  "Details": "Kvit-N is a preparation of Vitamin B1, B6 and B12.\n",
  "Dosage": "",
  "Available": [
    "Kvit-N 100/200/0.2 mg Tab.:Contains thiamine l00 mg + pyridoxine 200 mg + cyanocobalamin 0.2 mg/tablet."
  ],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Kvit-th",
  "Manufacturer": "Chemico",
  "OrigName": "thiamine HCl",
  "Details": "Kvit-th is a preparation of thiamine HCl.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "Vitamins"
  ]
}
{
  "Name": "Kynol d",
  "Manufacturer": "Eskayef",
  "OrigName": "dexketoprofen",
  "Details": "Kynol d is a preparation of dexketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS"
  ]
}
{
  "Name": "Kynol tr",
  "Manufacturer": "Eskayef",
  "OrigName": "ketoprofen",
  "Details": "Kynol tr is a preparation of ketoprofen.\n",
  "Dosage": "",
  "Available": [],
  "Classification": [
    "ANTIINFLAMMATORY AND ANTIRHEUMATIC PRODUCTS",
    "TOPICAL PRODUCTS FOR JOINT AND MUSCULAR PAIN"
  ]
}
