﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MedicineData
{
    class Program
    {
        private static WebClient client = new WebClient();
        
        private static string urlBase = "http://www.rxbd.info/Controller/";
        private static string urlExt = "Controller?action=search&alphabet=";
        private static string[] alphabets = { "T", "U", "V", "W", "X", "Y", "Z" }; 
        
        private static string XPath_MedNames="//div[@id='content']//a";
        private static string XPath_MedDetails = "//div[@id='content']//div[@id='sectionTitle']";
        private static string XPath_DetailsTitle = "//div[@id='content']//div[@id='topicTitle']//span";

        static void Main(string[] args)
        {
            Console.WriteLine("Initializing NerdCats Medicine Scraper");
            Console.WriteLine("***************************************");

            JsonSerializer Serializer = new JsonSerializer();
            HtmlWeb htWeb = new HtmlWeb();

            int count = 0;

            foreach (var alpha in alphabets)
            {
                
                Console.WriteLine("Initiating Medicine List download for the alphabet " + alpha);
                HtmlDocument htmldoc = htWeb.Load(urlBase + urlExt + alpha);

                Console.WriteLine("Download done, parsing Medicine Names");
                HtmlNodeCollection MedNames = htmldoc.DocumentNode.SelectNodes(XPath_MedNames);

                List<Medicine> Medlist = new List<Medicine>();
                //List<Medicine> Medlist = (from item in MedNames
                //  select new Medicine() { Name = item.InnerText.Trim() }).ToList();

                Console.WriteLine("Fetched " + MedNames.Count + " medicines");
                Console.WriteLine("Parsing medicine details");
                foreach (var item in MedNames)
                {

                    try
                    {

                        string link = string.Empty;
                        if (item.HasAttributes)
                            link = urlBase + item.Attributes["href"].Value.Trim();

                        HtmlWeb dtWeb = new HtmlWeb();
                        var detailsData = dtWeb.Load(link.Replace("amp;", ""));

                        Medicine tempMedicine = new Medicine();
                        tempMedicine.Name = item.InnerText.Trim();

                        HtmlNodeCollection detailsNodes = detailsData.DocumentNode.SelectNodes(XPath_MedDetails);

                        try
                        {
                            HtmlNode OriginalNameNode = detailsData.DocumentNode.SelectNodes(XPath_DetailsTitle).FirstOrDefault();
                            tempMedicine.OrigName = OriginalNameNode.InnerText.Trim();
                            tempMedicine.OrigName = tempMedicine.OrigName.Substring(1, tempMedicine.OrigName.Length - 2);
                        }
                        catch { }

                        foreach (var details in detailsNodes)
                        {
                            if (details.InnerText.Trim().ToLower().Contains("manufacturer"))
                            {
                                tempMedicine.Manufacturer = details.NextSibling.InnerText.Trim();
                            }

                            if (details.InnerText.Trim().ToLower().Contains("used"))
                            {
                                string context = "";
                                HtmlNode nextNode = details.NextSibling;
                                while (nextNode.Name.Equals("#text") || nextNode.Name.Equals("b"))
                                {

                                    context += nextNode.InnerText.Trim() + "\n";
                                    nextNode = nextNode.NextSibling;
                                }

                                tempMedicine.Details = context;
                            }

                            if (details.InnerText.Trim().ToLower().Contains("available as"))
                            {
                                string context = "";
                                HtmlNode nextNode = details.NextSibling;

                                while (!nextNode.Name.Equals("div"))
                                {
                                    if (nextNode.Name.Equals("b"))
                                    {
                                        if (!string.IsNullOrEmpty(context.Trim()))
                                            tempMedicine.Available.Add(context);
                                        context = "";
                                        context += nextNode.InnerText.Trim();
                                    }
                                    else
                                    {
                                        context += nextNode.InnerText.Trim();
                                    }

                                    nextNode = nextNode.NextSibling;

                                }


                            }

                            if (details.InnerText.Trim().ToLower().Contains("classification"))
                            {
                                string context = "";
                                HtmlNode nextNode = details.NextSibling;


                                while (!nextNode.Name.Equals("div"))
                                {
                                    if (nextNode.Name.Equals("a"))
                                    {
                                        nextNode = nextNode.NextSibling;
                                        continue;
                                    }

                                    if (!string.IsNullOrEmpty(nextNode.InnerText.Trim()))
                                        tempMedicine.Classification.Add(nextNode.InnerText.Trim());

                                    nextNode = nextNode.NextSibling;

                                }

                            }

                            if (details.InnerText.Trim().ToLower().Contains("dosage"))
                            {
                                string context = "";
                                HtmlNode nextNode = details.NextSibling;

                                while (!nextNode.Name.Equals("div"))
                                {
                                    context += nextNode.InnerText.Trim();
                                    nextNode = nextNode.NextSibling;
                                }
                                tempMedicine.Dosage = context;

                            }
                        }

                        Console.WriteLine("\n===============================");
                        Console.WriteLine("MEDICINE DETAILS:");
                        Console.WriteLine("\nNAME :" + tempMedicine.Name);
                        Console.WriteLine("\nORIGINAL NAME :" + tempMedicine.OrigName);
                        Console.WriteLine("\nMANUFACTURER :" + tempMedicine.Manufacturer);
                        Console.WriteLine("\nUSAGE :" + tempMedicine.Details);
                        Console.WriteLine("\nAVAILABLE AS :");
                        foreach (var dose in tempMedicine.Available)
                        {
                            Console.WriteLine(dose);
                        }
                        Console.WriteLine("\nCLASSIFICATIONS :");
                        foreach (var cls in tempMedicine.Classification)
                        {
                            Console.WriteLine(cls);
                        }

                        Console.WriteLine("\n===============================");

                        //Medlist.Add(tempMedicine);

                        using (StreamWriter Swriter = new StreamWriter(alpha+".txt", true))
                        using (JsonTextWriter Jwriter = new JsonTextWriter(Swriter))
                        {
                            Jwriter.Formatting = Formatting.Indented;
                            Serializer.Serialize(Jwriter, tempMedicine);
                            Swriter.WriteLine();
                            Console.WriteLine(tempMedicine.Name + " is added to file");
                        }
                    }
                    catch
                    {
                    }

                    count++;

                    Console.WriteLine("\n\nCONFIRMED " + count+ " Medicines ");

                }

                Console.WriteLine("***************************************");
                
            }
            
            
            Console.Read();


        }
    }

    internal class Medicine
    {
        public string Name { get; set; }
        
        public string Manufacturer { get; set; }
        public string OrigName { get; set; }
        public string Details { get; set; }
        public string Dosage { get; set; }
        public List<string> Available { get; set; }
        public List<string> Classification { get; set; }

        public Medicine()
        {
            Classification = new List<string>();
            Available = new List<string>();
        }
             
    }
}
